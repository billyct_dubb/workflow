import { TextSymbol } from './text'

export class OperationSymbol extends TextSymbol {
  render (): void {
    super.render()

    const width = this.width + 20
    const height = this.height + 20

    const bgElement = this.paper.rect(
      this.x - width / 2,
      this.y - height / 2,
      width,
      height
    )

    bgElement.attr({
      stroke: this.config.color,
      'stroke-width': this.config.lineWidth,
      fill: 'white'
    })

    this.group.toFront()

    this.group.push(bgElement)
  }
}
