import { RaphaelAttributes, RaphaelElement } from 'raphael'

import { Symbol } from './symbol'
import { TextType, TextObject, TextSymbolConfig } from '../interfaces'

export class TextSymbol extends Symbol {
  get text (): TextType {
    return (this.config as TextSymbolConfig).text
  }

  render (): void {
    const x = this.x
    let y = this.y

    if (typeof this.text === 'string') {
      this.renderText(x, y, this.text)
    }

    if (typeof this.text === 'object') {
      if (this.text instanceof Array) {
        for (const textObject of this.text) {
          const textElement = this.renderText(x, y, textObject)
          y += textElement.getBBox().height / 2 + 10
        }
      } else {
        this.renderText(x, y, this.text)
      }
    }

    const height = this.height

    // 重新调整位置
    this.group.forEach(element => {
      element.attr({
        y: element.getBBox().y2 - height / 2,
        'text-anchor': 'middle'
      })
    })
  }

  renderText (x: number, y: number, textObject: string|TextObject): RaphaelElement {
    if (typeof textObject === 'string') {
      textObject = {
        text: textObject
      }
    }

    const textElement = this.paper.text(
      x,
      y,
      textObject.text
    )

    let attr: Partial<RaphaelAttributes> = {
      'font-size': this.config.fontSize,
      fill: this.config.color,
      stroke: 'none',
      'text-anchor': 'start'
    }

    if (textObject.attr != null) {
      attr = {
        ...attr,
        ...textObject.attr
      }
    }

    textElement.attr(attr)

    this.group.push(textElement)

    return textElement
  }
}
