import { TextSymbol } from './text'
import { RaphaelPathSegment } from 'raphael'

export class ConditionSymbol extends TextSymbol {
  render (): void {
    super.render()

    const width = this.width + 80
    const height = this.height + 70

    const paths: readonly RaphaelPathSegment[] = [
      ['M', this.x - width / 2, this.y],
      ['L', this.x, this.y - height / 2],
      ['L', this.x + width / 2, this.y],
      ['L', this.x, this.y + height / 2],
      ['Z']
    ]

    const bgElement = this.paper.path(paths)
    bgElement.attr({
      fill: 'white',
      stroke: this.config.color,
      'stroke-width': this.config.lineWidth
    })

    this.group.toFront()
    this.group.push(bgElement)
  }
}
