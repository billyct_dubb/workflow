import { RaphaelAttributes } from 'raphael'

declare module 'workflow-builder.js' {

  interface TextObject {
    text: string
    attr?: Partial<RaphaelAttributes>
  }

  type TextType = string | TextObject | TextObject[]

  interface NodeData {
    id: number | string
    type: NodeType
    title: TextType
    tooltip?: TextType
    data: object
    children?: NodeData[]
  }

  type NodeEvent = (e: MouseEvent, node: NodeData) => void

  interface WorkflowConfig {
    color: string
    lineWidth: number
    fontSize: number

    creation?: {
      tooltip?: string
      onClick: NodeEvent
    }

    node?: {
      onClick: NodeEvent
    }
  }

  export enum NodeType {
    Start = 'start',
    Operation = 'operation',
    Condition = 'condition',
  }

  export class Workflow {
    constructor (container: HTMLElement, config?: Partial<WorkflowConfig>)
    render: (nodeData: NodeData, x?: number, y?: number) => void
    reload: (nodeData: NodeData) => void
    clean: () => void
  }
}
