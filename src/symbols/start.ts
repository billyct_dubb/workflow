import { TextSymbol } from './text'

export class StartSymbol extends TextSymbol {
  render (): void {
    super.render()

    const width = this.width + 20
    const height = this.height + 20

    const bgElement = this.paper.rect(
      this.x - width / 2,
      this.y - height / 2,
      width,
      height,
      20
    )

    bgElement.attr({
      stroke: '#3594d5',
      'stroke-width': this.config.lineWidth,
      fill: 'white'
    })

    this.group.toFront()
    this.group.push(bgElement)
  }
}
