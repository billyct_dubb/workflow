import Raphael, { RaphaelAxisAlignedBoundingBox, RaphaelPaper } from 'raphael'

import { ConditionNode, Node, OperationNode, StartNode } from './nodes'
import { NodeConfig, NodeData, NodeType, TextSymbolConfig, WorkflowConfig } from './interfaces'
import { configDefault } from './config'
import { ConditionSymbol, OperationSymbol, StartSymbol, Symbol } from './symbols'

export class Workflow {
  readonly paper: RaphaelPaper
  private readonly container: HTMLElement
  private readonly config: WorkflowConfig

  nodes: Node[] = []

  private minX: number = 100
  private minY: number = 100
  private maxX: number = 100
  private maxY: number = 100

  constructor (container: HTMLElement, config: Partial<WorkflowConfig> = {}) {
    this.container = container
    this.container.style.position = 'relative'
    // @ts-expect-error
    this.paper = new Raphael(container)
    this.config = {
      ...configDefault,
      ...config
    }
  }

  private resizePaper (node: Node): void {
    const { x, y, x2, y2 } = node.group.getBBox()

    if (x < this.minX) {
      this.minX = x
    }

    if (y < this.minY) {
      this.minY = y
    }

    if (x2 > this.maxX) {
      this.maxX = x2
    }

    if (y2 > this.maxY) {
      this.maxY = y2
    }

    const width = this.maxX - this.minX + this.config.lineWidth * 2
    const height = this.maxY - this.minY + this.config.lineWidth * 2

    this.paper.setViewBox(
      this.minX - this.config.lineWidth,
      this.minY - this.config.lineWidth,
      width,
      height,
      true
    )
    this.paper.setSize(width, height)
  }

  reload (nodeData: NodeData): void {
    this.clean()
    this.render(nodeData)
  }

  clean (): void {
    for (const node of this.nodes) {
      node.clean()
    }

    this.nodes = []
  }

  render (nodeData: NodeData, x?: number, y?: number): Node {
    const nodeConfig: NodeConfig = {
      ...this.config,
      paper: this.paper,
      x: x ?? this.minX,
      y: y ?? this.minY,
      data: nodeData
    }

    let node: Node

    switch (nodeData.type) {
      case NodeType.Start:
        node = new StartNode(nodeConfig)
        break
      case NodeType.Operation:
        node = new OperationNode(nodeConfig)
        break
      case NodeType.Condition:
        node = new ConditionNode(nodeConfig)
        break
    }

    this.resizePaper(node)

    node.nextNodes = this.renderNodeChildren(node)

    this.nodes.push(node)

    return node
  }

  private renderNodeChildren (node: Node): Node[] {
    const res: Node[] = []

    if (node instanceof StartNode || node instanceof OperationNode) {
      if (node.hasChildren()) {
        // @ts-expect-error
        const child = node.data.children[0]
        if (child != null) {
          const symbolBBox = Workflow.getNodeSymbolBBox(child)
          // @ts-expect-error
          const deltaY = node.symbol.height / 2 + node.line.height + symbolBBox.height / 2
          const childNode = this.render(child, node.x, node.y + deltaY)

          childNode.prevNode = node

          res.push(childNode)
        }
      }
    }

    if (node instanceof ConditionNode) {
      const dataYes = node.childrenYes()
      if (dataYes != null) {
        const symbolBBox = Workflow.getNodeSymbolBBox(dataYes)
        // @ts-expect-error
        const deltaY = node.symbol.height / 2 + node.lineYes.height + symbolBBox.height / 2
        const childNode = this.render(dataYes, node.x, node.y + deltaY)

        childNode.prevNode = node

        const x2Next = childNode.getNextX2()
        // 最右边的 x2 数值大于当前 condition node 的 x2 数值，则需要重新渲染 line no
        if (x2Next > node.bbox.x2) {
          node.rerenderLineNo(x2Next - node.bbox.x2 + 50)
        }

        res.push(childNode)
      }

      const dataNo = node.childrenNo()
      if (dataNo != null) {
        const symbolBBox = Workflow.getNodeSymbolBBox(dataNo)
        // @ts-expect-error
        const deltaX = node.symbol.width / 2 + node.lineNo.width + symbolBBox.width / 2
        const childNode = this.render(dataNo, node.x + deltaX, node.y)

        childNode.prevNode = node

        res.push(childNode)
      }
    }

    return res
  }

  private static getNodeSymbolBBox (data: NodeData): RaphaelAxisAlignedBoundingBox {
    const paper = Raphael(0, 0, 0, 0)
    if ('style' in paper.canvas) {
      paper.canvas.style.visibility = 'hidden'
    }

    let symbol: Symbol

    const symbolConfig: TextSymbolConfig = {
      ...configDefault,
      paper,
      x: 0,
      y: 0,
      text: data.title
    }

    switch (data.type) {
      case NodeType.Operation:
        symbol = new OperationSymbol(symbolConfig)
        break
      case NodeType.Condition:
        symbol = new ConditionSymbol(symbolConfig)
        break
      case NodeType.Start:
        symbol = new StartSymbol(symbolConfig)
        break
    }

    const res = symbol.bbox

    paper.remove()

    return res
  }
}
