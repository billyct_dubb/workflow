export const configDefault = {
  color: '#555555',
  lineWidth: 3,
  fontSize: 14,
  draggable: false
}
