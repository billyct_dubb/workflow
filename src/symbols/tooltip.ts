import { TextSymbol } from './text'

export class TooltipSymbol extends TextSymbol {
  render (): void {
    super.render()

    const width = this.width + 20
    const height = this.height + 20

    const bgElement = this.paper.rect(
      this.x - width / 2,
      this.y - height / 2,
      width,
      height
    )

    bgElement.attr({
      fill: '#383838',
      'stroke-width': 0
    })

    this.group.attr({
      fill: '#ffffff'
    })
    this.group.toFront()

    this.group.push(bgElement)
  }
}
