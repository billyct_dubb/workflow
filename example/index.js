import {Workflow} from '../lib/esm/workflow.js'

function htmlDecode (input) {
  const e = document.createElement('div');
  e.innerHTML = input;
  return e.childNodes[0].nodeValue;
}

const container = document.getElementById('app')

const workflow = new Workflow(container, {
  creation: {
    tooltip: 'Add New Action',
    onClick: (e, node) => alert(`create ${node.title}`),
  },
  node: {
    onClick: (e, node) => alert(node.title),
  },
})
workflow.render({
  id: 1,
  type: 'start',
  title: [
    'Workflow Triggered',
    {
      text: '105,432 contacts',
      attr: {
        'font-size': 10
      }
    }
  ],
  children: [
    {
      id: 2,
      type: 'operation',
      title: 'Add Tag',
      tooltip: 'added_tag_1 \/ added_tag_2',
      children: [
        {
          id: 3,
          type: 'operation',
          title: 'Delay',
          tooltip: '10 minutes',
          children: [
            {
              id: 4,
              type: 'condition',
              title: 'If/Then Branch',
              tooltip: "CTA Clicked",
              data: {
                yes: 5,
                no: 6,
              },
              children: [
                {
                  id: 6,
                  type: 'operation',
                  title: 'Remove Tag',
                  tooltip: 'added_tag_1',
                  children: [
                    {
                      id: 7,
                      type: 'condition',
                      title: 'If/Then Branch',
                      tooltip: "CTA Clicked",
                      data: {
                        yes: 8,
                      },
                      children: [
                        {
                          id: 8,
                          type: 'operation',
                          title: 'Send Campaign',
                        }
                      ]
                    }
                  ]
                },
                {
                  id: 5,
                  type: 'operation',
                  title: 'Send Campaign',
                  tooltip: [
                    'Demo workflow campaign',
                    {
                      text: '10,234 emails sent',
                      attr: {
                        'font-size': 10
                      }
                    },
                    {
                      text: htmlDecode('&#xf2b6; 54% &#xf0a6; 6% &#xf06e; 6% &#xf144; 6%'),
                      attr: {
                        'font-size': 10,
                        'font-family': '"Font Awesome 5 Free"'
                      }
                    },
                  ],
                  children: [
                    {
                      id: 11,
                      type: 'condition',
                      title: 'If/Then Branch',
                      tooltip: "CTA Clicked",
                      data: {
                        no: 12,
                      },
                      children: [
                        {
                          id: 12,
                          type: 'operation',
                          title: 'Send Campaign',
                        }
                      ]
                    }
                  ]
                },
              ],
            },
          ],
        },
      ],
    }
  ]
})