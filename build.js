const {build} = require('esbuild')

const isProduction = process.env.NODE_ENV === 'production'

const config = {
  entryPoints: ['src/index.ts'],
  format: 'esm',
  bundle: true,
  minify: false,
  sourcemap: true,
  watch: true,
  target: ['chrome60', 'firefox60', 'safari11', 'edge18'],
  outfile: 'lib/esm/workflow.js',
  ...isProduction && {
    minify: true,
    watch: false,
    sourcemap: false,
    external: ['raphael'],
  }
}

const cjsConfig = {
  ...config,
  format: 'cjs',
  outfile: 'lib/cjs/workflow.js',
}

build(config).catch(() => process.exit(1))
build(cjsConfig).catch(() => process.exit(1))