import { RaphaelAttributes, RaphaelPaper } from 'raphael'

export interface TextObject {
  text: string
  attr?: Partial<RaphaelAttributes>
}

export type TextType = string | TextObject | TextObject[]

export interface SymbolConfig {
  paper: RaphaelPaper
  // 中心点 x
  x: number
  // 中心点 y
  y: number
  color: string
  lineWidth: number
  fontSize: number
}

export interface TextSymbolConfig extends SymbolConfig {
  text: TextType
}

export interface LineSymbolConfig extends SymbolConfig {
  direction: 'bottom' | 'right'
  hasArrow: boolean
  text?: string
}

export interface NodeData {
  id: number | string
  type: NodeType
  title: TextType
  tooltip?: TextType
  data: object
  children?: NodeData[]
}

export interface ConditionData extends NodeData {
  data: {
    yes?: number
    no?: number
  }
  condition?: 'yes' | 'no'
}

export enum NodeType {
  Start = 'start',
  Operation = 'operation',
  Condition = 'condition',
}

export interface NodeConfig extends SymbolConfig, WorkflowConfig {
  data: NodeData
}

export type NodeEvent = (e: MouseEvent, node: NodeData) => void

export interface WorkflowConfig {
  color: string
  lineWidth: number
  fontSize: number

  draggable: boolean

  creation?: {
    tooltip?: string
    onClick: NodeEvent
  }

  node?: {
    onClick: NodeEvent
  }
}
