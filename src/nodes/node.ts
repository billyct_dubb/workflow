import { RaphaelPathSegment } from 'raphael'

import { CreationSymbol, LineSymbol, Symbol, TooltipSymbol } from '../symbols'
import { NodeConfig, NodeData, LineSymbolConfig, TextSymbolConfig, SymbolConfig } from '../interfaces'
import { debounce } from '../utils'

export abstract class Node extends Symbol {
  symbol: Symbol | undefined
  line: LineSymbol | undefined
  tooltip: TooltipSymbol | undefined
  creation: CreationSymbol | undefined

  prevNode: Node | undefined
  nextNodes: Node[] = []

  isDragging: boolean = false
  previousDx: number = 0
  previousDy: number = 0

  constructor (config: NodeConfig) {
    super(config)

    this.renderLine()
    this.renderTooltip()
    this.renderCreation()

    this.addToGroup(this.symbol)
    this.addToGroup(this.tooltip)
    this.addToGroup(this.line)
    this.addToGroup(this.creation)

    if (this.draggable) {
      this.dragAndDrop()
    }

    if (((this.nodeConfig.node?.onClick) != null) && this.symbol != null) {
      this.symbol.group.attr({
        cursor: 'pointer'
      })
      this.symbol.group.click(e => {
        if (this.isDragging) {
          return
        }

        this.nodeConfig.node?.onClick.apply(this, [e, this.data])
      })
    }

    if (((this.nodeConfig.creation?.onClick) != null) && this.creation != null) {
      this.creation.group.attr({
        cursor: 'pointer'
      })
      this.creation.group.click(e => {
        this.nodeConfig.creation?.onClick.apply(this, [e, this.data])
      })
    }
  }

  get nodeConfig (): NodeConfig {
    return this.config as NodeConfig
  }

  get data (): NodeData {
    return this.nodeConfig.data
  }

  get draggable (): boolean {
    return this.nodeConfig.draggable
  }

  protected addToGroup (symbol: Symbol | undefined): void {
    if (symbol != null) {
      symbol.group.forEach(element => {
        this.group.push(element)
      })
    }
  }

  clean (): void {
    this.group.forEach(element => {
      element.remove()
    })

    this.group.clear()
  }

  hasChildren (): boolean {
    return !(this.data.children == null || this.data.children.length === 0)
  }

  protected renderLine (lineSymbolConfig: Partial<LineSymbolConfig> = {}): void {
    if (this.symbol != null) {
      const config: LineSymbolConfig = {
        ...this.config,

        direction: 'bottom',
        hasArrow: this.hasChildren(),
        y: this.y + this.symbol.height / 2,

        ...lineSymbolConfig
      }

      this.line = new LineSymbol(config)
    }
  }

  protected renderCreation (symbolConfig: Partial<SymbolConfig> = {}): void {
    if (this.symbol != null) {
      const y = this.y + this.symbol.height / 2 + LineSymbol.size / 2

      this.creation = new CreationSymbol({
        ...this.config,
        y,

        ...symbolConfig
      })

      if (this.nodeConfig.creation?.tooltip != null) {
        const tooltipConfig: TextSymbolConfig = {
          ...this.config,
          text: this.nodeConfig.creation.tooltip,
          x: this.creation.x,
          y: this.creation.y
        }

        const tooltip = new TooltipSymbol(tooltipConfig)

        const deltaY = CreationSymbol.radius + tooltip.height / 2 + 10
        tooltip.group.translate(0, -deltaY)
        tooltip.group.hide()

        this.creation.group.hover(() => {
          tooltip.group.show()
        }, () => {
          tooltip.group.hide()
        })
      }
    }
  }

  protected renderTooltip (): void {
    if (this.symbol != null && this.data.tooltip != null) {
      const config: TextSymbolConfig = {
        ...this.config,
        text: this.data.tooltip
      }

      this.tooltip = new TooltipSymbol(config)

      const deltaY = this.tooltip.height / 2 + this.symbol.height / 2 + 10
      this.tooltip.group.translate(0, -deltaY)
      this.tooltip.group.hide()

      this.symbol.group.hover(() => {
        // @ts-expect-error
        this.tooltip.group.show()
      }, () => {
        // @ts-expect-error
        this.tooltip.group.hide()
      })
    }
  }

  /**
   * 获取最右边的 x2 数值
   */
  getNextX2 (): number {
    let x2 = this.bbox.x2
    this.nextNodes.forEach(node => {
      const x2Next = node.getNextX2()

      if (x2Next > x2) {
        x2 = x2Next
      }
    })

    return x2
  }

  onDragMove (dx: number, dy: number): void {
    this.isDragging = true

    const deltaX = dx - this.previousDx
    const deltaY = dy - this.previousDy

    this.symbol?.group.translate(deltaX, deltaY)
    this.tooltip?.group.translate(deltaX, deltaY)

    this.moveLine(deltaX, deltaY)
    if (this.prevNode != null) {
      this.prevNode.moveLineWithNextNode(deltaX, deltaY, this)
    }

    this.previousDx = dx
    this.previousDy = dy
  }

  protected moveLine (deltaX: number, deltaY: number): void {
    if (this.symbol != null) {
      // @ts-expect-error
      const path: RaphaelPathSegment[] = this.line?.group[0].attr('path')
      const xStart = this.symbol.bbox.x2 - this.symbol.width / 2
      const yStart = this.symbol.bbox.y2

      path[0] = ['M', xStart, yStart]
      // @ts-expect-error
      this.line?.group.attr({ path })
      if (this.hasChildren()) {
        this.creation?.group.translate(deltaX / 2, deltaY / 2)
      }
    }
  }

  protected moveLineWithNextNode (deltaX: number, deltaY: number, nextNode: Node): void {
    if (nextNode.symbol != null) {
      // @ts-expect-error
      const path: RaphaelPathSegment[] = this.line?.group[0].attr('path')

      const xEnd = nextNode.symbol.bbox.x + nextNode.symbol.width / 2
      const yEnd = nextNode.symbol.bbox.y

      path[1] = ['L', xEnd, yEnd]
      // @ts-expect-error
      this.line?.group.attr({ path })
      this.creation?.group.translate(deltaX / 2, deltaY / 2)
    }
  }

  onDragStart (): void {
    this.previousDx = 0
    this.previousDy = 0
  }

  onDragUp (): void {
    setTimeout(() => {
      this.isDragging = false
    }, 1)
  }

  dragAndDrop (): void {
    if (this.symbol != null) {
      this.symbol.group.drag(
        debounce(this.onDragMove.bind(this), 5),
        this.onDragStart.bind(this),
        this.onDragUp.bind(this)
      )
    }
  }
}
