import { RaphaelElement } from 'raphael'

import { Symbol } from './symbol'
import { LineSymbolConfig } from '../interfaces'

export class LineSymbol extends Symbol {
  textElement: RaphaelElement | undefined

  static size: number = 100

  get lineSymbolConfig (): LineSymbolConfig {
    return this.config as LineSymbolConfig
  }

  get direction (): string {
    return this.lineSymbolConfig.direction
  }

  get hasArrow (): boolean {
    return this.lineSymbolConfig.hasArrow
  }

  get text (): string | undefined {
    return this.lineSymbolConfig.text
  }

  get isRightDirection (): boolean {
    return this.direction === 'right'
  }

  render (): void {
    let size = LineSymbol.size

    if (!this.hasArrow) {
      size = size / 2
    }

    let path = `M${this.x} ${this.y} L${this.x} ${this.y + size}`

    if (this.isRightDirection) {
      path = `M${this.x} ${this.y} L${this.x + size} ${this.y}`
    }

    const lineElement = this.paper.path(path)

    lineElement.attr({
      stroke: this.config.color,
      'stroke-width': this.config.lineWidth
    })

    if (this.hasArrow) {
      lineElement.attr({
        'arrow-end': 'block'
      })
    }

    this.group.push(lineElement)

    this.renderText()
  }

  renderText (): void {
    if (this.text != null) {
      let x = this.x + 15
      let y = this.y + 10

      if (this.isRightDirection) {
        x = this.x + 10
        y = this.y - 10
      }

      const textElement = this.paper.text(x, y, this.text)

      textElement.attr({
        'font-size': this.config.fontSize,
        fill: this.config.color,
        stroke: 'none'
      })

      this.textElement = textElement

      this.group.push(textElement)
    }
  }
}
