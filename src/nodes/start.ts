import { Node } from './node'
import { StartSymbol } from '../symbols'
import { TextSymbolConfig } from '../interfaces'

export class StartNode extends Node {
  render (): void {
    const config: TextSymbolConfig = {
      ...this.config,
      text: this.data.title
    }

    this.symbol = new StartSymbol(config)
  }
}
