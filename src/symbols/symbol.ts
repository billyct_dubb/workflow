import {
  RaphaelAxisAlignedBoundingBox,
  RaphaelPaper,
  RaphaelSet
} from 'raphael'

import { SymbolConfig } from '../interfaces'

export abstract class Symbol {
  readonly config: SymbolConfig

  group: RaphaelSet

  constructor (config: SymbolConfig) {
    this.config = config
    this.group = this.paper.set()

    this.render()
  }

  get paper (): RaphaelPaper {
    return this.config.paper
  }

  get x (): number {
    return this.config.x
  }

  set x (v: number) {
    this.config.x += v
  }

  get y (): number {
    return this.config.y
  }

  set y (v: number) {
    this.config.y += v
  }

  get bbox (): RaphaelAxisAlignedBoundingBox {
    return this.group.getBBox()
  }

  get width (): number {
    return this.bbox.width
  }

  get height (): number {
    return this.bbox.height
  }

  abstract render (): void
}
