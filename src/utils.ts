export function debounce (func: any, delay: number): any {
  let timer: number
  return (...args: any) => {
    if (timer > 0) {
      func(...args)
    }
    clearTimeout(timer)
    timer = setTimeout(() => func(...args), delay)
  }
}
