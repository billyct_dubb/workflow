import { Node } from './node'
import { ConditionSymbol, CreationSymbol, LineSymbol } from '../symbols'
import { ConditionData, NodeConfig, NodeData, TextSymbolConfig } from '../interfaces'
import { RaphaelPathSegment } from 'raphael'

export class ConditionNode extends Node {
  lineYes: LineSymbol | undefined
  lineNo: LineSymbol | undefined

  creationYes: CreationSymbol | undefined
  creationNo: CreationSymbol | undefined

  constructor (config: NodeConfig) {
    super(config)

    this.addToGroup(this.lineYes)
    this.addToGroup(this.lineNo)
    this.addToGroup(this.creationYes)
    this.addToGroup(this.creationNo)

    const onClick = this.nodeConfig.creation?.onClick
    if (onClick != null) {
      if (this.creationYes != null) {
        this.creationYes.group.attr({
          cursor: 'pointer'
        })
        this.creationYes.group.click(e => {
          const data = this.conditionData
          data.condition = 'yes'

          onClick?.apply(this, [e, data])
        })
      }

      if (this.creationNo != null) {
        this.creationNo.group.attr({
          cursor: 'pointer'
        })
        this.creationNo.group.click(e => {
          const data = this.conditionData
          data.condition = 'no'

          onClick?.apply(this, [e, data])
        })
      }
    }
  }

  render (): void {
    const config: TextSymbolConfig = {
      ...this.config,
      text: this.data.title
    }

    this.symbol = new ConditionSymbol(config)
  }

  get conditionData (): ConditionData {
    return this.data as ConditionData
  }

  childrenYes (): NodeData|undefined {
    if (this.hasChildren()) {
      const id = this.conditionData.data.yes
      if (id != null) {
        // @ts-expect-error
        return this.conditionData.children.find(c => c.id === id)
      }
    }

    return undefined
  }

  childrenNo (): NodeData|undefined {
    if (this.hasChildren()) {
      const id = this.conditionData.data.no
      if (id != null) {
        // @ts-expect-error
        return this.conditionData.children.find(c => c.id === id)
      }
    }

    return undefined
  }

  hasChildrenYes (): boolean {
    return this.childrenYes() != null
  }

  hasChildrenNo (): boolean {
    return this.childrenNo() != null
  }

  nextNodeYes (): Node|undefined {
    const child = this.childrenYes()
    if (child != null) {
      return this.nextNodes.find(n => n.data.id === child.id)
    }

    return undefined
  }

  nextNodeNo (): Node|undefined {
    const child = this.childrenNo()
    if (child != null) {
      return this.nextNodes.find(n => n.data.id === child.id)
    }

    return undefined
  }

  protected renderLine (): void {
    if (this.symbol != null) {
      super.renderLine({
        text: 'Yes',
        direction: 'bottom',
        hasArrow: this.hasChildrenYes()
      })

      this.lineYes = this.line
      this.line = undefined

      super.renderLine({
        x: this.x + this.symbol.width / 2,
        y: this.y,
        text: 'No',
        direction: 'right',
        hasArrow: this.hasChildrenNo()
      })

      this.lineNo = this.line
      this.line = undefined
    }
  }

  protected renderCreation (): void {
    if (this.symbol != null) {
      super.renderCreation()
      this.creationYes = this.creation
      this.creation = undefined

      super.renderCreation({
        x: this.x + this.symbol.width / 2 + LineSymbol.size / 2,
        y: this.y
      })
      this.creationNo = this.creation
      this.creation = undefined
    }
  }

  /**
   * 重新渲染右边的 No 箭头线，根据增加的长度
   * @param deltaX
   */
  rerenderLineNo (deltaX: number): void {
    if (this.symbol != null) {
      const xStart = this.x + this.symbol.width / 2
      const xEnd = xStart + LineSymbol.size + deltaX
      this.lineNo?.group.attr({
        path: `M${xStart} ${this.y} L${xEnd} ${this.y}`
      })

      const xCenter = xStart + (xEnd - xStart) / 2
      this.creationNo?.group.attr({
        x: xCenter,
        cx: xCenter
      })
    }
  }

  moveLine (deltaX: number, deltaY: number): void {
    this.moveLineYes(deltaX, deltaY)
    this.moveLineNo(deltaX, deltaY)
  }

  moveLineYes (deltaX: number, deltaY: number): void {
    if (this.symbol != null) {
      // @ts-expect-error
      const path: RaphaelPathSegment[] = this.lineYes?.group[0].attr('path')
      const xStart = this.symbol.bbox.x2 - this.symbol.width / 2
      const yStart = this.symbol.bbox.y2

      path[0] = ['M', xStart, yStart]
      // @ts-expect-error
      this.lineYes?.group.attr({ path })
      this.lineYes?.textElement?.attr({
        x: xStart + 15,
        y: yStart + 10
      })

      if (this.hasChildrenYes()) {
        this.creationYes?.group.translate(deltaX / 2, deltaY / 2)
      }
    }
  }

  moveLineNo (deltaX: number, deltaY: number): void {
    if (this.symbol != null) {
      // @ts-expect-error
      const path: RaphaelPathSegment[] = this.lineNo?.group[0].attr('path')
      const xStart = this.symbol.bbox.x2
      const yStart = this.symbol.bbox.y2 - this.symbol.height / 2

      path[0] = ['M', xStart, yStart]
      // @ts-expect-error
      this.lineNo?.group.attr({ path })

      this.lineNo?.textElement?.attr({
        x: xStart + 10,
        y: yStart - 10
      })

      if (this.hasChildrenNo()) {
        this.creationNo?.group.translate(deltaX / 2, deltaY / 2)
      }
    }
  }

  moveLineWithNextNode (deltaX: number, deltaY: number, nextNode: Node): void {
    if (nextNode.data.id === this.nextNodeYes()?.data.id) {
      this.moveLineYesWithNextNode(deltaX, deltaY)
    }

    if (nextNode.data.id === this.nextNodeNo()?.data.id) {
      this.moveLineNoWithNextNode(deltaX, deltaY)
    }
  }

  moveLineYesWithNextNode (deltaX: number, deltaY: number): void {
    const nextNode = this.nextNodeYes()
    if (nextNode?.symbol != null) {
      // @ts-expect-error
      const path: RaphaelPathSegment[] = this.lineYes?.group[0].attr('path')

      const xEnd = nextNode.symbol.bbox.x + nextNode.symbol.width / 2
      const yEnd = nextNode.symbol.bbox.y

      path[1] = ['L', xEnd, yEnd]
      // @ts-expect-error
      this.lineYes?.group.attr({ path })

      this.creationYes?.group.translate(deltaX / 2, deltaY / 2)
    }
  }

  moveLineNoWithNextNode (deltaX: number, deltaY: number): void {
    const nextNode = this.nextNodeNo()
    if (nextNode?.symbol != null) {
      // @ts-expect-error
      const path: RaphaelPathSegment[] = this.lineNo?.group[0].attr('path')

      const xEnd = nextNode.symbol.bbox.x
      const yEnd = nextNode.symbol.bbox.y + nextNode.symbol.height / 2

      path[1] = ['L', xEnd, yEnd]
      // @ts-expect-error
      this.lineNo?.group.attr({ path })

      this.creationNo?.group.translate(deltaX / 2, deltaY / 2)
    }
  }
}
