import { Symbol } from './symbol'

export class CreationSymbol extends Symbol {
  static radius: number = 8

  render (): void {
    const circleElement = this.paper.circle(this.x, this.y, CreationSymbol.radius)

    circleElement.attr({
      fill: 'white',
      stroke: this.config.color,
      'stroke-width': this.config.lineWidth
    })

    const textElement = this.paper.text(this.x, this.y, '+')
    textElement.attr({
      'font-size': this.config.fontSize,
      fill: this.config.color
    })

    this.group.push(circleElement, textElement)
  }
}
