import { Node } from './node'
import { OperationSymbol } from '../symbols'
import { TextSymbolConfig } from '../interfaces'

export class OperationNode extends Node {
  render (): void {
    const config: TextSymbolConfig = {
      ...this.config,
      text: this.data.title
    }

    this.symbol = new OperationSymbol(config)
  }
}
